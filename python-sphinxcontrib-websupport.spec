Name:           python-sphinxcontrib-websupport
Version:        2.0.0
Release:        1
Summary:        Sphinx API for Web Apps

License:        BSD
URL:            http://sphinx-doc.org/
Source0:        %{pypi_source sphinxcontrib_websupport}
BuildArch:      noarch

%description
sphinxcontrib-websupport provides a Python API to easily integrate Sphinx
documentation into your Web application.


%package -n     python3-sphinxcontrib-websupport
Summary:        Sphinx API for Web Apps
%{?python_provide:%python_provide python3-sphinxcontrib-websupport}
BuildRequires:  python3-devel python3-docutils python3-setuptools
BuildRequires:  python3-jinja2 python3-mock python3-pytest python3-six
BuildRequires:  python3-sphinx python3-sqlalchemy python3-whoosh
BuildRequires:  python3-pip python3-wheel python3-flit-core 
Requires:       python3-docutils python3-jinja2 python3-six  python3-sphinx
Requires:       python3-sqlalchemy python3-whoosh

%description -n python3-sphinxcontrib-websupport
sphinxcontrib-websupport provides a Python API to easily integrate Sphinx
documentation into your Web application.

%prep
%autosetup -n sphinxcontrib_websupport-%{version} -p1
rm -rf sphinxcontrib-websupport.egg-info

%build
%pyproject_build

%install
%pyproject_install

%check
export PYTHONPATH=%{buildroot}%{python3_sitelib}
%{__python3} tests/test_websupport.py

%files -n python3-sphinxcontrib-websupport
%doc LICENCE.rst README.rst
%{python3_sitelib}/sphinxcontrib/websupport
%{python3_sitelib}/sphinxcontrib_websupport-2.0.0.dist-info

%changelog
* Thu Nov 07 2024 Ge Wang <wang__ge@126.com> - 2.0.0-1
- Update to version 2.0.0

* Mon Nov 14 2022 yaoxin <yaoxin30@h-partners.com> - 1.2.4-3
- Modify invalid Source

* Thu Oct 27 2022 zhangruifang <zhangruifang1@h-partners.com> - 1.2.4-2
- Rebuild for next release

* Mon Feb 1 2021 yuanxin <yuanxin24@huawei.com> - 1.2.4-1
- Upgrade version to 1.2.4

* Wed Jul 29 2020 tianwei <tianwei12g@huawei.com> - 1.2.2-1
- update release to 1.2.2

* Thu Nov 28 2019 Wanjiankang <wanjiankang@huawei.com> - 1.0.1-11
- Initial RPM
